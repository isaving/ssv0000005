////Version: v0.0.1
package routers

import (
	"git.forms.io/isaving/sv/ssv0000005/controllers"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/universe/common/event_handler/register"
	"github.com/astaxie/beego"
)

// @Desc Select the appropriate transaction route according to TOPIC
// @Author
// @Date 2020-12-12
func InitRouter() error {

	eventRouteReg := register.NewEventHandlerRegister()

	bc := beego.AppConfig

	eventRouteReg.Router(bc.String(constant.TopicPrefix+"ssv0000005"),
		&controllers.Ssv0000005Controller{}, "Ssv0000005")

	return nil
}

// @Desc transaction router for swagger
// @Author
// @Date 2020-12-12
func init() {

	ns := beego.NewNamespace("isaving/v1/xxxxxx",
		beego.NSNamespace("/xxxxxx",
			beego.NSInclude(
				&controllers.Ssv0000005Controller{},
			),
		),
	)
	beego.AddNamespace(ns)
}
