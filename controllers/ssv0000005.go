//Version: v0.0.1
package controllers

import (
	"git.forms.io/isaving/models"
	"git.forms.io/isaving/sv/ssv0000005/services"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/controllers"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/dts/client/aspect"
	"git.forms.io/universe/solapp-sdk/log"
	"runtime/debug"
)

type Ssv0000005Controller struct {
	controllers.CommTCCController
}

func (*Ssv0000005Controller) ControllerName() string {
	return "Ssv0000005Controller"
}

// @Desc ssv0000005 controller
// @Author
// @Date 2020-12-12
func (c *Ssv0000005Controller) Ssv0000005() {

	defer func() {
		if r := recover(); r != nil {
			err := errors.Errorf(constant.SYSPANIC, "Ssv0000005Controller.Ssv0000005 Catch panic %v", r)
			log.Errorf("Error: %v, Stack: [%s]", err, debug.Stack())
			c.SetServiceError(err)
		}
	}()

	ssv0000005I := &models.SSV0000005I{}
	if err := models.UnPackRequest(c.Req.Body, ssv0000005I); err != nil {
		c.SetServiceError(err)
		return
	}
	if err := ssv0000005I.Validate(); err != nil {
		log.Errorf("Request message field validate failed, error:[%v]", err)
		c.SetServiceError(err)
		return
	}
	ssv0000005 := &services.Ssv0000005Impl{}
	ssv0000005.New(c.CommTCCController)
	ssv0000005.Ssv000005I = ssv0000005I
	ssv0000005Compensable := services.Ssv0000005Compensable

	proxy, err := aspect.NewDTSProxy(ssv0000005, ssv0000005Compensable, c.DTSCtx)
	if err != nil {
		log.Error("Register DTS Proxy failed. %v", err)
		c.SetServiceError(errors.Errorf(constant.PROXYREGFAILD, "Register DTS Proxy failed. %v", err))
		return
	}

	rets := proxy.Do(ssv0000005I)

	if len(rets) < 1 {
		log.Error("DTS proxy executed service failed, not have any return")
		c.SetServiceError(errors.Errorf(constant.PROXYFAILD, "DTS proxy executed service failed, %v", "not have any return"))
		return
	}

	if e := rets[len(rets)-1].Interface(); e != nil {
		log.Errorf("DTS proxy executed service failed %v", err)
		c.SetServiceError(e)
		return
	}

	rsp := rets[0].Interface()
	if ssv0000005O, ok := rsp.(*models.SSV0000005O); ok {
		if responseBody, err := models.PackResponse(ssv0000005O); err != nil {
			c.SetServiceError(err)
		} else {
			c.SetAppBody(responseBody)
		}
	}
}

// @Title Ssv0000005 Controller
// @Description ssv0000005 controller
// @Param Ssv0000005 body model.SSV0000005I true body for SSV0000005 content
// @Success 200 {object} model.SSV0000005O
// @router /create [post]
func (c *Ssv0000005Controller) SWSsv0000005() {
	//Here is to generate API documentation, no need to implement methods
}
