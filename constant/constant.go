//Version: v0.0.1
package constant

//define error code
const (
	DLS_TYPE_CMM        = "CMM"
	DLS_TYPE_ELEMENT_ID = "common"
	DLS_TYPE_CUS        = "CUS"
	SV100014            = "sv100014"
	SV900004            = "sv900004"
	SV900009            = "sv900009"
	SV100020            = "sv100020"
	SV900010            = "sv900010"
	SV100019            = "sv100019"
)

const (
	ERRCODE01 = "SV12000005"
	ERRCODE02 = "SV12000006"
	ERRCODE03 = "SV12000002"
	ERRCODE04 = "SV12000003"
	ERRCODE05 = "SV12000007"
	ERRCODE06 = "SV12000008"
	ERRCODE07 = "SV12000009"
	ERRCODE08 = "SV12000010"
	ERRCODE09 = ""
)
