package services

import (
	"git.forms.io/legobank/legoapp/config"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/context"
	"git.forms.io/legobank/legoapp/controllers"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/legobank/legoapp/remote"
	"git.forms.io/universe/comm-agent/client"
	common "git.forms.io/universe/comm-agent/common/protocol"
	"git.forms.io/universe/dts/client/aspect"
	"git.forms.io/universe/solapp-sdk/comm"
	"git.forms.io/universe/solapp-sdk/compensable"
	"git.forms.io/universe/solapp-sdk/log"
	"git.forms.io/universe/solapp-sdk/util"
	"github.com/astaxie/beego"
)

var remoteCall = comm.NewRemoteClientFactory().CreateClient()

type CommonTCCService struct {
	aspect.DTSBaseService
	CommonService
	RemoteCallInterface remote.RemoteCallInterface
}

type CommonService struct {
	ServiceCtx          *context.ServiceContext
	SrcAppProps         map[string]string
	RemoteCallInterface remote.RemoteCallInterface
}

func (s *CommonService) New(c controllers.CommController) {
	s.ServiceCtx = c.Ctx
	s.SrcAppProps = c.Req.AppProps
	if nil == c.RemoteCallInterface {
		s.RemoteCallInterface = &remote.DefaultRemoteCallImpl{}
	} else {
		s.RemoteCallInterface = c.RemoteCallInterface
	}
}

func (s *CommonTCCService) New(c controllers.CommTCCController) {
	s.ServiceCtx = c.Ctx
	s.SrcAppProps = c.Req.AppProps
	if nil == c.RemoteCallInterface {
		s.RemoteCallInterface = &remote.DefaultRemoteCallImpl{}
	} else {
		s.RemoteCallInterface = c.RemoteCallInterface
	}
}

func requestService(
	dtsCtx *compensable.TxnCtx,
	dstDcn, serviceKey string,
	requestData []byte, srcAppProps map[string]string) (responseData []byte, dstAppProps map[string]string, err error) {

	var dcn string
	if dstDcn == "000000" {
		dcn = ""
	} else if "" != dstDcn {
		dcn = dstDcn
	}
	if nil == srcAppProps {
		srcAppProps = make(map[string]string)
	}
	srcAppProps[constant.SRCBIZSEQNO] = util.GenerateSerialNo("1")

	request := &client.UserMessage{
		AppProps: srcAppProps,
		Body:     requestData,
	}
	response := &client.UserMessage{}
	topic := beego.AppConfig.String(serviceKey + "::mesh")
	if err := remoteCall.SyncCall(dtsCtx, config.ServiceConf.Organization, dcn, serviceKey, request, response, 0); err != nil {
		return nil, nil,
			errors.Errorf(constant.REMOTEFAILD, "Remote call %s failed, error: %v", topic, err)
	}

	log.Debugf("End remoteCall topic: [%v], AppProps: %++v, Body: %v", topic, response.AppProps, string(response.Body))
	updateAppProps(request.AppProps, response.AppProps)
	if response.AppProps[constant.RETSTATUS] == "F" {
		return response.Body, response.AppProps, errors.New(response.AppProps[constant.RETMESSAGE], response.AppProps[constant.RETMSGCODE])
	}

	return response.Body, response.AppProps, nil
}

// Deprecated: Use RequestSyncServiceWithDCN instead.
func (a *CommonService) RequestServiceWithDCN(
	dstDcn, serviceKey string,
	requestData []byte, srcAppProps map[string]string) (responseData []byte, dstAppProps map[string]string, err error) {

	return requestService(nil, dstDcn, serviceKey, requestData, srcAppProps)
}

// Deprecated: Use RequestSyncServiceElementKey instead.
func (a *CommonService) RequestServiceElementKey(
	elementType, elementId, serviceKey string,
	requestData []byte, srcAppProps map[string]string) (responseData []byte, dstAppProps map[string]string, err error) {
	log.Debugf("start remoteCall serviceKey %v,srcAppProps%v,requestData %v", serviceKey, srcAppProps, string(requestData))
	if srcAppProps == nil {
		srcAppProps = make(map[string]string)
	}
	srcAppProps[common.DLS_ELEMENT_TYPE] = elementType
	srcAppProps[common.DLS_ELEMENT_ID] = elementId
	srcAppProps[common.TARGET_DCN] = ""

	return requestService(nil, "", serviceKey, requestData, srcAppProps)
}

// Deprecated: Use RequestSyncServiceWithDCN instead.
func (a *CommonTCCService) RequestServiceWithDCN(
	dstDcn, serviceKey string,
	requestData []byte, srcAppProps map[string]string) (responseData []byte, dstAppProps map[string]string, err error) {

	return requestService(a.DTSCtx, dstDcn, serviceKey, requestData, srcAppProps)
}

// Deprecated: Use RequestSyncServiceElementKey instead.
func (a *CommonTCCService) RequestServiceElementKey(
	elementType, elementId, serviceKey string,
	requestData []byte, srcAppProps map[string]string) (responseData []byte, dstAppProps map[string]string, err error) {
	log.Debugf("start remoteCall serviceKey %v,srcAppProps%v,requestData %v", serviceKey, srcAppProps, string(requestData))

	if srcAppProps == nil {
		srcAppProps = make(map[string]string)
	}
	srcAppProps[common.DLS_ELEMENT_TYPE] = elementType
	srcAppProps[common.DLS_ELEMENT_ID] = elementId
	srcAppProps[common.TARGET_DCN] = ""

	return requestService(a.DTSCtx, "", serviceKey, requestData, srcAppProps)
}

//func requestAsyncService(dtsCtx *compensable.TxnCtx,
//	dstDcn, serviceKey string,
//	requestData []byte, srcAppProps map[string]string) error {
//
//	var dcn string
//	if dstDcn == "000000" {
//		dcn = ""
//	} else if "" != dstDcn {
//		dcn = dstDcn
//	}
//	if nil == srcAppProps {
//		srcAppProps = make(map[string]string)
//	}
//	srcAppProps[constant.SRCBIZSEQNO] = util.GenerateSerialNo("1")
//
//	request := &client.UserMessage{
//		AppProps: srcAppProps,
//		Body:     requestData,
//	}
//
//	topic := beego.AppConfig.String(serviceKey + "::mesh")
//	if err := remoteCall.AsyncCall(dtsCtx, config.ServiceConf.Organization, dcn, serviceKey, request); err != nil {
//		return errors.Errorf(constant.REMOTEFAILD, "Remote call %s failed, error: %v", topic, err)
//	}
//
//	log.Debugf("End remoteCall topic: [%v]", topic)
//
//	return nil
//}

func (a *CommonService) RequestAsyncServiceWithDCN(
	dstDcn, serviceKey string, requestData []byte) error {
	//
	//if topic, err := checkServiceKey(serviceKey); err != nil {
	//	return err
	//} else {
	//	log.Debugf("Start remoteCall topic [%v], destDcn [%v], srcAppProps [%++v], requestData [%v]",
	//		topic, dstDcn, a.SrcAppProps, string(requestData))
	//}
	//
	//return requestAsyncService(nil, dstDcn, serviceKey, requestData, a.SrcAppProps)
	return a.RemoteCallInterface.RequestAsyncServiceWithDCN(a.SrcAppProps, dstDcn, serviceKey, requestData)
}

func (a *CommonService) RequestAsyncServiceElementKey(
	elementType, elementId, serviceKey string, requestData []byte) error {
	//
	//if topic, err := checkServiceKey(serviceKey); err != nil {
	//	return err
	//} else {
	//	log.Debugf("Start remoteCall topic [%v], elementType [%v], elementId [%v], srcAppProps [%++v], requestData [%v]",
	//		topic, elementType, elementId, a.SrcAppProps, string(requestData))
	//}
	//
	//if a.SrcAppProps == nil {
	//	a.SrcAppProps = make(map[string]string)
	//}
	//a.SrcAppProps[common.DLS_ELEMENT_TYPE] = elementType
	//a.SrcAppProps[common.DLS_ELEMENT_ID] = elementId
	//a.SrcAppProps[common.TARGET_DCN] = ""
	//return requestAsyncService(nil, "", serviceKey, requestData, a.SrcAppProps)
	return a.RemoteCallInterface.RequestAsyncServiceElementKey(a.SrcAppProps, elementType, elementId, serviceKey, requestData)
}

func (a *CommonService) RequestSyncServiceWithDCN(
	dstDcn, serviceKey string, requestData []byte) (responseData []byte, err error) {

	//if topic, err := checkServiceKey(serviceKey); err != nil {
	//	return nil, err
	//} else {
	//	log.Debugf("Start remoteCall topic [%v], destDcn [%v], srcAppProps [%++v], requestData [%v]",
	//		topic, dstDcn, a.SrcAppProps, string(requestData))
	//}
	//
	//return requestSyncService(nil, dstDcn, serviceKey, requestData, a.SrcAppProps)
	return a.RemoteCallInterface.RequestSyncServiceWithDCN(nil, a.SrcAppProps, dstDcn, serviceKey, requestData)
}

func (a *CommonService) RequestSyncServiceElementKey(
	elementType, elementId, serviceKey string, requestData []byte) (responseData []byte, err error) {

	//if topic, err := checkServiceKey(serviceKey); err != nil {
	//	return nil, err
	//} else {
	//	log.Debugf("Start remoteCall topic [%v], elementType [%v], elementId [%v], srcAppProps [%++v], requestData [%v]",
	//		topic, elementType, elementId, a.SrcAppProps, string(requestData))
	//}
	//if a.SrcAppProps == nil {
	//	a.SrcAppProps = make(map[string]string)
	//}
	//a.SrcAppProps[common.DLS_ELEMENT_TYPE] = elementType
	//a.SrcAppProps[common.DLS_ELEMENT_ID] = elementId
	//a.SrcAppProps[common.TARGET_DCN] = ""
	//
	//return requestSyncService(nil, "", serviceKey, requestData, a.SrcAppProps)
	return a.RemoteCallInterface.RequestSyncServiceElementKey(nil, a.SrcAppProps, elementType, elementId, serviceKey, requestData)
}

func (a *CommonTCCService) RequestSyncServiceWithDCN(
	dstDcn, serviceKey string, requestData []byte) (responseData []byte, err error) {

	//if topic, err := checkServiceKey(serviceKey); err != nil {
	//	return nil, err
	//} else {
	//	log.Debugf("Start remoteCall topic [%v], destDcn [%v], srcAppProps [%++v], requestData [%v]",
	//		topic, dstDcn, a.SrcAppProps, string(requestData))
	//}
	//return requestSyncService(a.DTSCtx, dstDcn, serviceKey, requestData, a.SrcAppProps)

	return a.RemoteCallInterface.RequestSyncServiceWithDCN(a.DTSCtx, a.SrcAppProps, dstDcn, serviceKey, requestData)
}

func (a *CommonTCCService) RequestSyncServiceElementKey(
	elementType, elementId, serviceKey string, requestData []byte) (responseData []byte, err error) {
	return a.RemoteCallInterface.RequestSyncServiceElementKey(a.DTSCtx, a.SrcAppProps, elementType, elementId, serviceKey, requestData)
}

func updateAppProps(appProps, rspAppProps map[string]string) {
	appProps[constant.TRGBIZSEQNO] = rspAppProps[constant.SRCBIZSEQNO]
	appProps[constant.LASTACTENTRYNO] = rspAppProps[constant.LASTACTENTRYNO]
	appProps[common.DLS_ELEMENT_TYPE] = ""
	appProps[common.DLS_ELEMENT_ID] = ""
}

func checkServiceKey(serviceKey string) (topic string, err error) {

	topic = beego.AppConfig.String(serviceKey + "::mesh")
	if topic == "" {
		return "", errors.Errorf(constant.NOTFOUNDTOPIC, "Can't found destination topic. serviceKey: [%v]", serviceKey)
	}
	return topic, nil
}
