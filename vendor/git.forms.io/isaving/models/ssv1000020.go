//Version: v0.0.1
package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type SSV1000020I struct {
	TxnAccountingFlows []TxnAccountingFlows `json:"TxnAccountingFlows"`
}

type TxnAccountingFlows struct {
	TxnDate    				string `validate:"required" json:"TxnDate"`    		 	//交易日期
	GlobalBizSeqNo 			string `validate:"required" json:"GlobalBizSeqNo"`  	//全局业务流水号
	TranBrno            	string  `validate:"required" json:"TranBrno"`     		//交易机构
	TranChannel    			string  `validate:"required" json:"TranChannel"`   		//交易发起渠道
	BussSys       	 		string  `validate:"required" json:"BussSys"` 			//业务系统
	TranReqCode          	string  `validate:"required" json:"TranReqCode"`   		//交易请求码
	HandleType         	 	string  `validate:"required" json:"HandleType"`         //处理类型
	HandleTypeSeq           int64   `validate:"required" json:"HandleTypeSeq"`      //处理类型序号
	AcctDiff    			string  `validate:"required" json:"AcctDiff"`         	//本方账户区分
	AcctingOrgId       		string  `validate:"required" json:"AcctingOrgId"`       //本方账务行所
	AcctNo      			string  `json:"AcctNo"`                    				//本方客户账号
	ContNo       			string  `json:"ContNo"`                    				//本方合约号
	Cur         			string  `json:"Cur"`                      				//本方币别
	Amt        				float64  `json:"Amt"`                     				//本方发生额
	QuivalenceAmt         	float64  `json:"QuivalenceAmt"`                      	//本方等值发生额
	ExchRate        		float64  `json:"ExchRate"`                     			//本方汇率
	TranSerialNo        	string  `json:"TranSerialNo"`                     		//原交易流水号
	SrcTranDate        		string  `json:"SrcTranDate"`                     		//原交易日期
}

type SSV1000020O struct {
	TotalFlowNum int `json:"TotalFlowNum"`
}

// @Desc Build request message
func (o *SSV1000020I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *SSV1000020I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *SSV1000020O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *SSV1000020O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *SSV1000020I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

func (*SSV1000020I) GetServiceKey() string {
	return "ssv1000020"
}
