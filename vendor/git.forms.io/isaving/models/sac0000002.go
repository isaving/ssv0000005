//Version: v0.01
package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type SAC0000002I struct {
     AList []string `json:"AList" valid:"Required"`  //核算账号数组
}

type SAC0000002O struct {
AResult []Sac0002Record `json:"AResult"`

}

type Sac0002Record struct {
	Account        string //合约号
	AccountingId   string //核算账号
	AmtFrozen      float64 //冻结金额
	AmtCurrent     float64 //当前余额
	AmtLast        float64 //上期余额
	OpenDate       string //开户日期
	LastActiveDate string //最后活动日期
	AccStatus      string //账户状态
	AvalAmount     float64 //可用余额
}

// @Desc Build request message
func (o *SAC0000002I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *SAC0000002I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *SAC0000002O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *SAC0000002O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *SAC0000002I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

func (*SAC0000002I) GetServiceKey() string {
	return "SAC0000002"
}
