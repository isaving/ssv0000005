//Version: v0.0.1
package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type SSV9000010I struct {
	TransationDate          string  `description:"transaction date"`                         //交易日期
	TransationTime          string  `description:"transaction hour"`                         //交易时间
	GlobalBizseqNo          string  `validate:"required" description:"Global serial number"` //全局流水号
	SrcBizseqNo             string  `description:"Business serial number"`                   //业务流水号
	SrcTimeStamp            string  `description:"Source system working date and time"`      //源系统工作日期时间
	SrcSysId                string  `description:"Source system encoding"`                   //源系统编码
	SrcDcn                  string  `description:"Source system DCN"`                        //源系统DCN
	OrgChannelType          string  `description:"Transaction initiation channel"`           //交易发起渠道
	TxDeviceId              string  `description:"Terminal number"`                          //终端号
	TxDeptCode              string  `description:"Trading house"`                            //交易行所
	BnAcc                   string  `description:"Accounting office"`                        //账务行所
	TransationEm            string  `description:"Transaction teller"`                       //交易柜员
	TransationType          string  `description:"means of transaction"`                     //交易方式
	TransationAmt           float64 `description:"The transaction amount"`                   //交易金额
	PairNm                  string  `description:"Set number"`                               //套号
	BatchNm                 string  `description:"batch number"`                             //批号
	TradeFlag               string  `description:"Anti-trading flag"`                        //反交易标志
	ReverseTradeFlag        string  `description:"Reversal transaction sign"`                //冲正交易标志
	OriginalTransactionDate string  `description:"Original transaction date"`                //原交易日期
	OriginalGlobalBizseqNo  string  `description:"Original global serial number"`            //原全局流水号
	OriginalSrcBizseqNo     string  `description:"Original business serial number"`          //原业务流水号
	TranResult              string  `description:"Processing status"`                        //处理状态
	ErrorCode               string  `description:"error code"`                               //错误码
	LastUpDatetime          string  `description:"Last update date and time"`                //最后更新日期时间
	LastUpBn                string  `description:"Last update"`                              //最后更新行所
	LastUpEm                string  `description:"Last update teller"`                       //最后更新柜员
}

type SSV9000010O struct {
}

// @Desc Build request message
func (o *SSV9000010I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *SSV9000010I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *SSV9000010O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *SSV9000010O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *SSV9000010I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

func (*SSV9000010I) GetServiceKey() string {
	return "ssv9000010"
}
