//Version: v0.0.1
package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type SSV1000019I struct {
	SysId             string                 `json:"sysId"`             //渠道标识 暂定ILNS 乐高智能贷款
	BizSn             string                 `json:"bizSn"`             //业务流水号
	ObjectSendList    IL1LSR01IItemList      `json:"objectSendList"`    //发送对象内容集合
	ObjectReceiveList IL1LSR01IItemList      `json:"objectReceiveList"` //接收对象内容集合
	LoanProdtNo       string                 `json:"loanProdtNo"`       //产品号
	ApplyScena        string                 `json:"applyScena"`        //适用场景大类
	TextTmplTypCd     string                 `json:"textTmplTypCd"`     //模板业务类型代码 1001-Transaction Statement 1002-Transaction History 1003-Repayment Plan 1004-Late Payment Notification
	OrgId             string                 `json:"orgId"`             //机构id
	Area              string                 `json:"area"`              //地区
	ChannelNumber     string                 `json:"channelNumber"`     //渠道号
	EventId           string                 `json:"eventId"`           //事件ID TopicID
	SendObjectType    string                 `json:"sendObjectType"`    //发送对象类型（01-机构，02-客户经理）
	TextTmplDetailCd  string                 `json:"textTmplDetailCd"`  //模板业务类型细类
	MsgDate           string                 `json:"msgDate"`           //日期
	MsgTime           string                 `json:"msgTime"`           //时间
	AttachFileIDList  []string               `json:"attachFileIDList"`  //附件文件ID列表
	Body              map[string]interface{} `json:"body"`              //内容
}

type SSV1000019O struct {
	ReturnCode   string `json:"returnCode"`   //错误码：0-邮件发送成功
	ReturnMsg    string `json:"returnMsg"`    //响应信息："SUCCESS"-发送成功
	DeliveryTime string `json:"deliveryTime"` //发件时间，格式：YYYYMMDDHH24MMSS
	ResponseTime string `json:"responseTime"` //响应时间，格式：YYYYMMDDHH24MMSS
	RepData      string `json:"repData"`      //邮件发送结果：true-成功，false-失败
}

type IL1LSR01IItemList []IL1LSR01IItem

type IL1LSR01IItem struct {
	ObjectKeyType string `json:"objectKeyType"` // 对象键字类型 01-客户编号 02-账号 03-证件
	ObjectKey     string `json:"objectKey"`     // 对象键字 客户编号 账号 证件类型+证件号码
	SerialNo      int    `json:"serialNo"`      // 排序号
}

// @Desc Build request message
func (o *SSV1000019I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *SSV1000019I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *SSV1000019O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *SSV1000019O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *SSV1000019I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

func (*SSV1000019I) GetServiceKey() string {
	return "ssv1000019"
}
