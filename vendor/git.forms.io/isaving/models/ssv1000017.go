//Version: v0.0.1
package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type SSV1000017I struct {
	Account string  `json:"Account"` //核算账号
	Amount  float64 `json:"Amount"`  //交易金额
	TrnDate string  `json:"TrnDate"` //交易日期
	CdFlag  string  `json:"CdFlag"`  //借贷标志
	//当cdFlag=C-贷时	amtCurrent=amtCurrent+交易金额
	//当cdFlag=D-借时	amtCurrent=amtCurrent-交易金额，同时如果amtCurrent <0 的话就报报错“账户余额不足”
}

type SSV1000017O struct {
	AmtFreezing  float64 `json:"AmtFreezing"`  //冻结金额
	AmtCurrent   float64 `json:"AmtCurrent"`   //当前余额
	AmtLast      float64 `json:"AmtLast"`      //上期余额
	AmtAvaliable float64 `json:"AmtAvaliable"` //可用余额   可用余额=当前余额-冻结金额-预留金额
	AccStatus    float64 `json:"AccStatus"`    //账户状态
}

// @Desc Build request message
func (o *SSV1000017I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *SSV1000017I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *SSV1000017O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *SSV1000017O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *SSV1000017I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

func (*SSV1000017I) GetServiceKey() string {
	return "ssv1000017"
}
