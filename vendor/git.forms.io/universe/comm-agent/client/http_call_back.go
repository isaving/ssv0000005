//
// Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.
//

package client

import (
	"fmt"
	log "git.forms.io/universe/comm-agent/common/log"
	"git.forms.io/universe/common/serializer"
	"github.com/buaazp/fasthttprouter"
	"github.com/go-errors/errors"
	"github.com/valyala/fasthttp"
	"net/http"
)

var (
	// Define callback function type variables, comm-sdk callback after receiving the message
	callbackHandFunc func(UserMessage) (*UserMessage, error)
	server           *fasthttp.Server
)

// CallbackFuncRegister provides an application registration callback method
func CallbackFuncRegister(handler func(UserMessage) (*UserMessage, error)) {
	callbackHandFunc = handler
}

// callbackHandlerForFastHttp Provides a fasthttp mode callback method
func callbackHandlerForFastHttp(ctx *fasthttp.RequestCtx) {
	requestBody := ctx.PostBody()
	protoMsg, err := serializer.String2protoMsg(string(requestBody))
	if nil != err {
		log.Errorf("callbackHandlerForFastHttp:proto message Unmarshal failed, error=%v", err)
		fmt.Fprintf(ctx, "callbackHandlerForFastHttp:unmarshal request failed, stauts code:%d", http.StatusInternalServerError)
		return
	}

	msg := ProtocolMsgToUserMsg(&protoMsg)
	var response string

	reply, err := callbackHandFunc(msg)
	if nil != err {
		response = serializer.ProtoMsg2String(nil, err)
		fmt.Fprint(ctx, response)
		return
	}

	if nil != reply {
		res := UserMsgToProtocolMsg(reply)
		response = serializer.ProtoMsg2String(&res, nil)
	} else if protoMsg.NeedReply {
		err := errors.Errorf("callbackHandlerForFastHttp:reply message is nil,please check")
		response = serializer.ProtoMsg2String(nil, err)
	} else {
		retMsg := &UserMessage{}
		res := UserMsgToProtocolMsg(retMsg)
		response = serializer.ProtoMsg2String(&res, nil)
	}

	fmt.Fprint(ctx, response)
}

// getClientStat gets the current client status (used for fast http)
func getClientStatus(ctx *fasthttp.RequestCtx) {
	ctx.WriteString("ok")
}

// startUserMsgCallbackServer start callback service
// default callback http port is 18082
// callback url path is "/v1/newmsg"
func startUserMsgCallbackServer(port int) {
	//http.HandleFunc("/v1/newmsg", callbackHandlerForHttp)
	if port == 0 {
		port = 18082
	}

	serverAddr := fmt.Sprintf("0.0.0.0:%d", port)
	log.Debugf("startUserMsgCallbackServer: listen addr=%s", serverAddr)
	router := fasthttprouter.New()
	router.POST("/v1/newmsg", callbackHandlerForFastHttp)
	router.GET("/v1/client/status", getClientStatus)

	server = &fasthttp.Server{
		Handler:            router.Handler,
		MaxRequestBodySize: 1024 * 1024 * 1024,
	}
	if err := server.ListenAndServe(serverAddr); err != nil {
		log.Error("startUserMsgCallbackServer: start fasthttp failed:", err.Error())
		panic(err)
	}
}

func ShutdownClientListen() {
	if nil != server {
		log.Info("Start shutdown the client listen...")
		if err := server.Shutdown(); nil != err {
			log.Errorf("Shutdown the client listen failed, error = %v", err)
		} else {
			log.Info("Shutdown the client listen successfully!")
		}
	}
}
