//Version: v0.0.1
package services

import (
	"git.forms.io/isaving/models"
	"testing"
)

var sv900009 = `{
    "errorCode": "0",
    "errorMsg": "success",
    "response": {

    }
}`

var sv100014 = `{
    "errorCode": "0",
    "errorMsg": "success",
    "response": {
		"TueTranAmt":100,
		"ReturnCode":"1"
    }
}`

var sv900004 = `{
    "errorCode": "0",
    "errorMsg": "success",
    "response": {
				"RecordTotNum":1,
				"Records":[
					{				
						"AgreementID":"1",
						"AgreementType":"1",
						"Currency":"cny"
					}	
				]
    		}
}`

var sv100020 = `{
    "errorCode": "0",
    "errorMsg": "success",
    "response": {
		"TotalFlowNum":1
    }
}`

var sv100019 = `{
    "errorCode": "0",
    "errorMsg": "success",
    "response": {
		"ReturnCode":"1",
		"ReturnMsg":"success",
		"DeliveryTime":"2020-12-15 20:10:20",
		"ResponseTime":"2020-12-15 20:10:30",
		"RepData":"true"
    }
}`

var sv900010 = `{
    "errorCode": "0",
    "errorMsg": "success",
    "response": {

    }
}`

func (this *Ssv0000005Impl) RequestSyncServiceElementKey(elementType, elementId, serviceKey string, requestData []byte) (responseData []byte, err error) {
	switch serviceKey {
	case "sv900009":
		responseData = []byte(sv900009)
	case "sv100014":
		responseData = []byte(sv100014)
	case "sv900004":
		responseData = []byte(sv900004)
	case "sv100020":
		responseData = []byte(sv100020)
	case "sv100019":
		responseData = []byte(sv100019)
	case "sv900010":
		responseData = []byte(sv900010)
	}

	return responseData, nil
}

func (this *Ssv0000005Impl) RequestAsyncServiceElementKey(elementType, elementId, serviceKey string, requestData []byte) (err error) {
	switch serviceKey {
	case "sv100019":
		return nil
	}

	return nil
}

func TestSsv0000005Impl_Ssv0000005(t *testing.T) {

	Ssv00000051Impl := new(Ssv0000005Impl)

	//资金来源2
	_, _ = Ssv00000051Impl.TrySsv0000005(&models.SSV0000005I{
		SourceFunds:   "2",
		PayAgrmt:      "1",
		PayAgrmtTyp:   "1",
		PayInAcc:      "1",
		PayMdsNM:      "1",
		PayMdsTyp:     "1",
		PayCusId:      "1",
		PayCusTy:      "1",
		PayCUR:        "1",
		CashTranFlag:  "1",
		PayAccuntNme:  "1",
		UsgCod:        "1",
		WdrwlMthd:     "1",
		NeedPswFlg:    "1",
		AccPsw:        "1",
		TranAmt:       0,
		GoingFunds:    "1",
		CltAgrmt:      "1",
		CltAgrmtTyp:   "1",
		CltInAcc:      "1",
		CltMdsNM:      "1",
		CltMdsTyp:     "1",
		CltCusId:      "1",
		CltCusTy:      "1",
		CltCUR:        "1",
		CltAccuntNme:  "1",
		Postscript:    "1",
		DlyArrvlAFlag: "1",
		TranTyp:       "1",
		ChkDTrnFlag:   "1",
		AmtFreAuth:    "1",
		ChkDTrnCode:   "1",
		BatchNm:       "1",
	})

	//资金来源1
	_, _ = Ssv00000051Impl.TrySsv0000005(&models.SSV0000005I{
		SourceFunds:   "1",
		PayAgrmt:      "1",
		PayAgrmtTyp:   "1",
		PayInAcc:      "1",
		PayMdsNM:      "1",
		PayMdsTyp:     "1",
		PayCusId:      "1",
		PayCusTy:      "1",
		PayCUR:        "1",
		CashTranFlag:  "1",
		PayAccuntNme:  "1",
		UsgCod:        "1",
		WdrwlMthd:     "1",
		NeedPswFlg:    "1",
		AccPsw:        "1",
		TranAmt:       0,
		GoingFunds:    "1",
		CltAgrmt:      "1",
		CltAgrmtTyp:   "1",
		CltInAcc:      "1",
		CltMdsNM:      "1",
		CltMdsTyp:     "1",
		CltCusId:      "1",
		CltCusTy:      "1",
		CltCUR:        "1",
		CltAccuntNme:  "1",
		Postscript:    "1",
		DlyArrvlAFlag: "1",
		TranTyp:       "1",
		ChkDTrnFlag:   "1",
		AmtFreAuth:    "1",
		ChkDTrnCode:   "1",
		BatchNm:       "1",
	})

	//资金来源1  介质号为空
	_, _ = Ssv00000051Impl.TrySsv0000005(&models.SSV0000005I{
		SourceFunds:   "1",
		PayAgrmt:      "1",
		PayAgrmtTyp:   "1",
		PayInAcc:      "1",
		PayMdsNM:      "",
		PayMdsTyp:     "",
		PayCusId:      "1",
		PayCusTy:      "1",
		PayCUR:        "1",
		CashTranFlag:  "1",
		PayAccuntNme:  "1",
		UsgCod:        "1",
		WdrwlMthd:     "1",
		NeedPswFlg:    "1",
		AccPsw:        "1",
		TranAmt:       0,
		GoingFunds:    "1",
		CltAgrmt:      "1",
		CltAgrmtTyp:   "1",
		CltInAcc:      "1",
		CltMdsNM:      "1",
		CltMdsTyp:     "1",
		CltCusId:      "1",
		CltCusTy:      "1",
		CltCUR:        "1",
		CltAccuntNme:  "1",
		Postscript:    "1",
		DlyArrvlAFlag: "1",
		TranTyp:       "1",
		ChkDTrnFlag:   "1",
		AmtFreAuth:    "1",
		ChkDTrnCode:   "1",
		BatchNm:       "1",
	})

	//资金来源3  合约号是空
	_, _ = Ssv00000051Impl.TrySsv0000005(&models.SSV0000005I{
		SourceFunds:   "3",
		PayAgrmt:      "",
		PayAgrmtTyp:   "",
		PayInAcc:      "1",
		PayMdsNM:      "1",
		PayMdsTyp:     "1",
		PayCusId:      "1",
		PayCusTy:      "1",
		PayCUR:        "1",
		CashTranFlag:  "1",
		PayAccuntNme:  "1",
		UsgCod:        "1",
		WdrwlMthd:     "1",
		NeedPswFlg:    "1",
		AccPsw:        "1",
		TranAmt:       0,
		GoingFunds:    "1",
		CltAgrmt:      "1",
		CltAgrmtTyp:   "1",
		CltInAcc:      "1",
		CltMdsNM:      "1",
		CltMdsTyp:     "1",
		CltCusId:      "1",
		CltCusTy:      "1",
		CltCUR:        "1",
		CltAccuntNme:  "1",
		Postscript:    "1",
		DlyArrvlAFlag: "1",
		TranTyp:       "1",
		ChkDTrnFlag:   "1",
		AmtFreAuth:    "1",
		ChkDTrnCode:   "1",
		BatchNm:       "1",
	})

	//资金来源3
	_, _ = Ssv00000051Impl.TrySsv0000005(&models.SSV0000005I{
		SourceFunds:   "3",
		PayAgrmt:      "1",
		PayAgrmtTyp:   "1",
		PayInAcc:      "1",
		PayMdsNM:      "1",
		PayMdsTyp:     "1",
		PayCusId:      "1",
		PayCusTy:      "1",
		PayCUR:        "1",
		CashTranFlag:  "1",
		PayAccuntNme:  "1",
		UsgCod:        "1",
		WdrwlMthd:     "1",
		NeedPswFlg:    "1",
		AccPsw:        "1",
		TranAmt:       0,
		GoingFunds:    "1",
		CltAgrmt:      "1",
		CltAgrmtTyp:   "1",
		CltInAcc:      "1",
		CltMdsNM:      "1",
		CltMdsTyp:     "1",
		CltCusId:      "1",
		CltCusTy:      "1",
		CltCUR:        "1",
		CltAccuntNme:  "1",
		Postscript:    "1",
		DlyArrvlAFlag: "1",
		TranTyp:       "1",
		ChkDTrnFlag:   "1",
		AmtFreAuth:    "1",
		ChkDTrnCode:   "1",
		BatchNm:       "1",
	})

	//资金去向1
	_, _ = Ssv00000051Impl.TrySsv0000005(&models.SSV0000005I{
		SourceFunds:   "1",
		PayAgrmt:      "1",
		PayAgrmtTyp:   "1",
		PayInAcc:      "1",
		PayMdsNM:      "1",
		PayMdsTyp:     "1",
		PayCusId:      "1",
		PayCusTy:      "1",
		PayCUR:        "1",
		CashTranFlag:  "1",
		PayAccuntNme:  "1",
		UsgCod:        "1",
		WdrwlMthd:     "1",
		NeedPswFlg:    "1",
		AccPsw:        "1",
		TranAmt:       0,
		GoingFunds:    "1",
		CltAgrmt:      "1",
		CltAgrmtTyp:   "1",
		CltInAcc:      "1",
		CltMdsNM:      "1",
		CltMdsTyp:     "1",
		CltCusId:      "1",
		CltCusTy:      "1",
		CltCUR:        "1",
		CltAccuntNme:  "1",
		Postscript:    "1",
		DlyArrvlAFlag: "1",
		TranTyp:       "1",
		ChkDTrnFlag:   "1",
		AmtFreAuth:    "1",
		ChkDTrnCode:   "1",
		BatchNm:       "1",
	})

	//资金去向1  介质号是空
	_, _ = Ssv00000051Impl.TrySsv0000005(&models.SSV0000005I{
		SourceFunds:   "1",
		PayAgrmt:      "1",
		PayAgrmtTyp:   "1",
		PayInAcc:      "1",
		PayMdsNM:      "",
		PayMdsTyp:     "",
		PayCusId:      "1",
		PayCusTy:      "1",
		PayCUR:        "1",
		CashTranFlag:  "1",
		PayAccuntNme:  "1",
		UsgCod:        "1",
		WdrwlMthd:     "1",
		NeedPswFlg:    "1",
		AccPsw:        "1",
		TranAmt:       0,
		GoingFunds:    "1",
		CltAgrmt:      "1",
		CltAgrmtTyp:   "1",
		CltInAcc:      "1",
		CltMdsNM:      "1",
		CltMdsTyp:     "1",
		CltCusId:      "1",
		CltCusTy:      "1",
		CltCUR:        "1",
		CltAccuntNme:  "1",
		Postscript:    "1",
		DlyArrvlAFlag: "1",
		TranTyp:       "1",
		ChkDTrnFlag:   "1",
		AmtFreAuth:    "1",
		ChkDTrnCode:   "1",
		BatchNm:       "1",
	})

	//资金去向2
	_, _ = Ssv00000051Impl.TrySsv0000005(&models.SSV0000005I{
		SourceFunds:   "1",
		PayAgrmt:      "1",
		PayAgrmtTyp:   "1",
		PayInAcc:      "1",
		PayMdsNM:      "1",
		PayMdsTyp:     "1",
		PayCusId:      "1",
		PayCusTy:      "1",
		PayCUR:        "1",
		CashTranFlag:  "1",
		PayAccuntNme:  "1",
		UsgCod:        "1",
		WdrwlMthd:     "1",
		NeedPswFlg:    "1",
		AccPsw:        "1",
		TranAmt:       0,
		GoingFunds:    "2",
		CltAgrmt:      "1",
		CltAgrmtTyp:   "1",
		CltInAcc:      "1",
		CltMdsNM:      "1",
		CltMdsTyp:     "1",
		CltCusId:      "1",
		CltCusTy:      "1",
		CltCUR:        "1",
		CltAccuntNme:  "1",
		Postscript:    "1",
		DlyArrvlAFlag: "1",
		TranTyp:       "1",
		ChkDTrnFlag:   "1",
		AmtFreAuth:    "1",
		ChkDTrnCode:   "1",
		BatchNm:       "1",
	})

	//资金去向3
	_, _ = Ssv00000051Impl.TrySsv0000005(&models.SSV0000005I{
		SourceFunds:   "1",
		PayAgrmt:      "1",
		PayAgrmtTyp:   "1",
		PayInAcc:      "1",
		PayMdsNM:      "1",
		PayMdsTyp:     "1",
		PayCusId:      "1",
		PayCusTy:      "1",
		PayCUR:        "1",
		CashTranFlag:  "1",
		PayAccuntNme:  "1",
		UsgCod:        "1",
		WdrwlMthd:     "1",
		NeedPswFlg:    "1",
		AccPsw:        "1",
		TranAmt:       0,
		GoingFunds:    "3",
		CltAgrmt:      "1",
		CltAgrmtTyp:   "1",
		CltInAcc:      "1",
		CltMdsNM:      "1",
		CltMdsTyp:     "1",
		CltCusId:      "1",
		CltCusTy:      "1",
		CltCUR:        "1",
		CltAccuntNme:  "1",
		Postscript:    "1",
		DlyArrvlAFlag: "1",
		TranTyp:       "1",
		ChkDTrnFlag:   "1",
		AmtFreAuth:    "1",
		ChkDTrnCode:   "1",
		BatchNm:       "1",
	})

	//资金去向3  合约号是空
	_, _ = Ssv00000051Impl.TrySsv0000005(&models.SSV0000005I{
		SourceFunds:   "1",
		PayAgrmt:      "1",
		PayAgrmtTyp:   "1",
		PayInAcc:      "1",
		PayMdsNM:      "",
		PayMdsTyp:     "",
		PayCusId:      "1",
		PayCusTy:      "1",
		PayCUR:        "1",
		CashTranFlag:  "1",
		PayAccuntNme:  "1",
		UsgCod:        "1",
		WdrwlMthd:     "1",
		NeedPswFlg:    "1",
		AccPsw:        "1",
		TranAmt:       0,
		GoingFunds:    "3",
		CltAgrmt:      "1",
		CltAgrmtTyp:   "1",
		CltInAcc:      "1",
		CltMdsNM:      "1",
		CltMdsTyp:     "1",
		CltCusId:      "1",
		CltCusTy:      "1",
		CltCUR:        "1",
		CltAccuntNme:  "1",
		Postscript:    "1",
		DlyArrvlAFlag: "1",
		TranTyp:       "1",
		ChkDTrnFlag:   "1",
		AmtFreAuth:    "1",
		ChkDTrnCode:   "1",
		BatchNm:       "1",
	})

	_, _ = Ssv00000051Impl.ConfirmSsv0000005(&models.SSV0000005I{})

	_, _ = Ssv00000051Impl.CancelSsv0000005(&models.SSV0000005I{})

}
