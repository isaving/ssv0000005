//Version: v0.0.1
package services

import (
	"git.forms.io/isaving/models"
	"git.forms.io/isaving/sv/ssv0000005/constant"
	constant2 "git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/legobank/legoapp/services"
	dtsClient "git.forms.io/universe/dts/client"
	"git.forms.io/universe/solapp-sdk/log"
	"time"
)

var Ssv0000005Compensable = dtsClient.Compensable{
	TryMethod:     "TrySsv0000005",
	ConfirmMethod: "ConfirmSsv0000005",
	CancelMethod:  "CancelSsv0000005",
}

type Ssv0000005 interface {
	TrySsv0000005(*models.SSV0000005I) (*models.SSV0000005O, error)
	ConfirmSsv0000005(*models.SSV0000005I) (*models.SSV0000005O, error)
	CancelSsv0000005(*models.SSV0000005I) (*models.SSV0000005O, error)
}

type Ssv0000005Impl struct {
	services.CommonTCCService
	//TODO ADD Service Self Define Field
	Ssv100019O     *models.SSV1000019O
	Ssv100019I     *models.SSV1000019I
	Ssv900010O     *models.SSV9000010O
	Ssv900010I     *models.SSV9000010I
	Ssv100014O     *models.SSV1000014O
	Ssv100014I     *models.SSV1000014I
	Ssv900004O     *models.SSV9000004O
	Ssv900004I     *models.SSV9000004I
	Ssv900009O     *models.SSV9000009O
	Ssv900009I     *models.SSV9000009I
	Ssv000005O     *models.SSV0000005O
	Ssv000005I     *models.SSV0000005I
	Outssv90004O   *models.SSV9000004O //Out contact information
	Inssv90004O    *models.SSV9000004O //In contact information
	SrcBizSeqNo    string
	GlobalBizSeqNo string
}

// @Desc Ssv0000005 process
// @Author
// @Date 2020-12-12
func (impl *Ssv0000005Impl) TrySsv0000005(ssv0000005I *models.SSV0000005I) (ssv0000005O *models.SSV0000005O, err error) {

	impl.Ssv000005I = ssv0000005I
	if impl.SrcAppProps != nil {
		impl.SrcAppProps["_is_need_lookup"] = "1"
	}
	impl.GlobalBizSeqNo = impl.SrcAppProps[constant2.GLOBALBIZSEQNO]
	impl.SrcBizSeqNo = impl.SrcAppProps[constant2.SRCBIZSEQNO]

	//初始化CommonService(用于异步调用)
	impl.CommonService = services.CommonService{
		ServiceCtx:          impl.CommonTCCService.ServiceCtx,
		SrcAppProps:         impl.SrcAppProps,
		RemoteCallInterface: impl.RemoteCallInterface,
	}

	//Call transfer  flow to add
	//get custId as element key
	CustId := ""
	if impl.Ssv000005I.SourceFunds != "2" {
		CustId = impl.Ssv000005I.PayCusId
	} else {
		CustId = impl.Ssv000005I.CltCusId
	}

	//if SourceFunds = 2 (Internal account),don't deal with it
	switch impl.Ssv000005I.SourceFunds {
	case "1":
		//query contract info by medium id  and medium type
		if impl.Ssv000005I.PayMdsNM == "" || impl.Ssv000005I.PayMdsTyp == "" {
			return nil, errors.New("Media type and Media number can not be null", constant.ERRCODE03)
		}
		ContInfo := &models.SSV9000004I{
			QryTpy:       "2",                       //query type  0-medium  1-contract  2-medium purpose
			MediaType:    impl.Ssv000005I.PayMdsTyp, //medium type
			MediaNm:      impl.Ssv000005I.PayMdsNM,  //medium number
			Currency:     impl.Ssv000005I.PayCUR,    //currency
			CashTranFlag: impl.Ssv000005I.CashTranFlag,
			ChannelNm:    impl.SrcAppProps[constant2.ORGCHANNELTYPE],
			UsgCod:       impl.Ssv000005I.UsgCod,
		}
		sv90004O, err := impl.QueryContInfo(ContInfo, impl.Ssv000005I.PayCusId)
		if err != nil {
			return nil, errors.New("Query contract information failed", constant.ERRCODE05)
		}
		impl.Outssv90004O = sv90004O

	case "2":
		//Internal account,don't deal with it

	case "3":
		//query contract by contract id and contract type
		if impl.Ssv000005I.PayAgrmt == "" || impl.Ssv000005I.PayAgrmtTyp == "" {
			return nil, errors.New("Agreement type and Agreement number can not be null", constant.ERRCODE04)
		}
		ContInfo := &models.SSV9000004I{
			QryTpy:        "1",                         //query type
			AgreementId:   impl.Ssv000005I.PayAgrmt,    //contract number
			AgreementType: impl.Ssv000005I.PayAgrmtTyp, //contract type
		}
		sv90004O, err := impl.QueryContInfo(ContInfo, impl.Ssv000005I.PayCusId)
		if err != nil {
			return nil, errors.New("Query contract information failed", constant.ERRCODE05)
		}
		impl.Outssv90004O = sv90004O

	default:
		return nil, errors.New("Source Funds code is error", constant.ERRCODE06)
	}

	//if GoingFunds = 2 (Internal account),don't deal with it
	switch impl.Ssv000005I.GoingFunds {
	case "1":
		//query contract info by medium id  and medium type
		if impl.Ssv000005I.CltMdsNM == "" || impl.Ssv000005I.CltMdsTyp == "" {
			return nil, errors.New("Media type and Media number can not be null", constant.ERRCODE03)
		}
		ContInfo := &models.SSV9000004I{
			QryTpy:       "2",                       //query type  0-medium  1-contract
			MediaNm:      impl.Ssv000005I.CltMdsNM,  //medium number
			MediaType:    impl.Ssv000005I.CltMdsTyp, //medium type
			Currency:     impl.Ssv000005I.CltCUR,    //currency
			CashTranFlag: impl.Ssv000005I.CltCashTranFlag,
			ChannelNm:    impl.SrcAppProps[constant2.ORGCHANNELTYPE],
			UsgCod:       impl.Ssv000005I.CltUsgCod,
		}
		sv90004O, err := impl.QueryContInfo(ContInfo, impl.Ssv000005I.CltCusId)
		if err != nil {
			return nil, errors.New("Query contract information failed", constant.ERRCODE05)
		}
		impl.Inssv90004O = sv90004O

	case "2":
		//Internal account,don't deal with it

	case "3":
		//query contract by contract id and contract type
		if impl.Ssv000005I.CltAgrmt == "" || impl.Ssv000005I.CltAgrmtTyp == "" {
			return nil, errors.New("Agreement type and Agreement number can not be null", constant.ERRCODE04)
		}
		ContInfo := &models.SSV9000004I{
			QryTpy:        "1",                         //query type  0-medium  1-contract
			AgreementId:   impl.Ssv000005I.CltAgrmt,    //medium number
			AgreementType: impl.Ssv000005I.CltAgrmtTyp, //medium type
		}
		sv90004O, err := impl.QueryContInfo(ContInfo, impl.Ssv000005I.CltCusId)
		if err != nil {
			return nil, errors.New("Query contract information failed", constant.ERRCODE01)
		}
		impl.Inssv90004O = sv90004O

	default:
		return nil, errors.New("Going Funds code is error", constant.ERRCODE07)
	}

	if err = impl.InsertTransferFlow(CustId); err != nil {
		if errors.GetErrorCode(err) == "SV77000004" {
			//Duplicate primary key
			return nil, errors.New("Repeat transaction", constant.ERRCODE01)

		} else {
			//other error
			return nil, errors.New("Error in new transfer transaction flow", constant.ERRCODE02)
		}

	}

	//Call the intra bank transfer service  ssv1000014
	if err = impl.BankInTransfer(); err != nil {
		log.Debug("Update tran flow's tranResult to failed")
		if err := impl.UpdateTransferFlow(CustId, "F"); err != nil {
			return nil, err
		}
		return nil, err
	}

	//Call registration accounting flow service  ac050001
	if err = impl.InsertAcctFlow(); err != nil {
		log.Debug("Update tran flow's tranResult to failed")
		if err := impl.UpdateTransferFlow(CustId, "F"); err != nil {
			return nil, err
		}
		return nil, err
	}

	//Call transfer flow update   ssv9000010
	if err = impl.UpdateTransferFlow(CustId, "N"); err != nil {
		return nil, errors.New("Error update new transfer transaction flow", constant.ERRCODE08)
	}

	//call Call transfer flow insert(Both sides are contracts)
	if (impl.Ssv000005I.SourceFunds == "1" || impl.Ssv000005I.SourceFunds == "3") && (impl.Ssv000005I.GoingFunds == "1" || impl.Ssv000005I.GoingFunds == "3") {
		if err = impl.InsertTransferFlow(impl.Ssv000005I.CltCusId); err != nil {
			if errors.GetErrorCode(err) == "SV77000004" {
				//Duplicate primary key
				log.Debug("New transfer transaction flow, duplicate primary key")
			} else {
				//other error
				log.Debug("Update tran flow's tranResult to failed")
				if err := impl.UpdateTransferFlow(CustId, "F"); err != nil {
					return nil, err
				}
				return nil, errors.New("Error in new transfer transaction flow", constant.ERRCODE02)
			}

		}
	}

	//Asynchronous call dynamic account message notification  ssv1000019
	if err = impl.MoveAcctNotice(); err != nil {
		log.Debug("Update tran flow's tranResult to failed")
		if err := impl.UpdateTransferFlow(CustId, "F"); err != nil {
			return nil, err
		}
		return nil, err
	}

	ssv0000005O = &models.SSV0000005O{
		TueTranAmt: impl.Ssv100014O.TueTranAmt, //Actual amount of deduction
		ReturnCode: impl.Ssv100014O.ReturnCode,
	}

	return ssv0000005O, nil
}

//Personal contract information inquiry
func (impl *Ssv0000005Impl) QueryContInfo(sv900004I *models.SSV9000004I, CustId string) (*models.SSV9000004O, error) {

	//pack request
	reqBody, err := sv900004I.PackRequest()
	if nil != err {
		return nil, err
	}
	//call das get serial no
	resBody, err := impl.RequestSyncServiceElementKey(constant.DLS_TYPE_CUS, CustId, constant.SV900004, reqBody)
	if err != nil {
		return nil, err
	}

	sv900004O := models.SSV9000004O{}
	err = sv900004O.UnPackResponse(resBody)
	if err != nil {
		return nil, err
	}

	return &sv900004O, nil
}

//New transfer transaction flow
func (impl *Ssv0000005Impl) InsertTransferFlow(CustId string) error {
	ssv90009I := models.SSV9000009I{
		TransationDate:          time.Now().Format("2006-01-02"),
		TransationTime:          time.Now().Format("15:04:05"),
		GlobalBizSeqNo:          impl.GlobalBizSeqNo,
		SrcBizseqNo:             impl.SrcBizSeqNo,
		SrcTimeStamp:            impl.SrcAppProps[constant2.SRCTIMESTAMP],
		SrcSysId:                impl.SrcAppProps[constant2.SRCSYSID],
		SrcDcn:                  impl.SrcAppProps[constant2.SRCDCN],
		OrgChannelType:          impl.SrcAppProps[constant2.ORGCHANNELTYPE],
		TxDeviceId:              impl.SrcAppProps[constant2.TXDEVICEID],
		TxDeptCode:              impl.SrcAppProps[constant2.TXDEPTCODE],
		BnAcc:                   "",                                       //Accounting line
		TransationEm:            impl.SrcAppProps[constant2.TXUSERID],     //Trading teller
		TransationType:          impl.Ssv000005I.TranTyp,                  //Way to trade
		TransationAmt:           impl.Ssv000005I.TranAmt,                  //Transaction amount
		PairNm:                  "1",                                      //Set of no.
		BatchNm:                 "",                                       //Batch number
		TradeFlag:               "N",                                      //Counter trade mark
		ReverseTradeFlag:        "N",                                      //Counter the trade mark
		OriginalTransactionDate: "",                                       //Original trading date
		OriginalGlobalBizseqNo:  "",                                       //Original global running number
		OriginalSrcBizseqNo:     "",                                       //Original service serial number
		TranResult:              "",                                       //Processing state
		ErrorCode:               "",                                       //Error code
		ErrorDesc:               "",                                       //错误描述
		LastUpDatetime:          time.Now().Format("2006-01-02 15:04:05"), //Date and time last updated
		LastUpBn:                impl.SrcAppProps[constant2.TXDEPTCODE],   //The row office was last updated
		LastUpEm:                impl.SrcAppProps[constant2.TXDEVICEID],   //Last update teller
	}

	//pack request
	reqBody, err := ssv90009I.PackRequest()
	if nil != err {
		return err
	}
	//call das get serial no

	resBody, err := impl.RequestSyncServiceElementKey(constant.DLS_TYPE_CUS, CustId, constant.SV900009, reqBody)
	if err != nil {
		return err
	}

	sv900009O := models.SSV9000009O{}
	err = sv900009O.UnPackResponse(resBody)
	if err != nil {
		return err
	}
	return nil
}

//Intra bank transfer  BFS
func (impl *Ssv0000005Impl) BankInTransfer() error {
	ssv100014I := models.SSV1000014I{
		SourceFunds:     impl.Ssv000005I.SourceFunds, //Source of funds 1- Customer account;2- Internal account;3 - contract
		PayAgrmt:        impl.Ssv000005I.PayAgrmt,    //Payment Contract No.
		PayAgrmtTyp:     impl.Ssv000005I.PayAgrmtTyp, //Payment contract Type: personal current account;10002 - people
		PayInAcc:        impl.Ssv000005I.PayInAcc,    //Transfer out of internal account
		PayMdsNM:        impl.Ssv000005I.PayMdsNM,    //Payer's media number
		PayMdsTyp:       impl.Ssv000005I.PayMdsTyp,   //Type of media of payer
		PayCusId:        impl.Ssv000005I.PayCusId,    //Payer's customer number
		PayCusTy:        impl.Ssv000005I.PayCusTy,
		PayCUR:          impl.Ssv000005I.PayCUR,       //The payer is RMB 156- RMB
		CashTranFlag:    impl.Ssv000005I.CashTranFlag, //Bank note exchange mark C- Bank note households;T - hui families;-
		PayAccuntNme:    impl.Ssv000005I.PayAccuntNme, //Payment account name
		UsgCod:          impl.Ssv000005I.UsgCod,       //Purpose code: EAC- Electronic account
		WdrwlMthd:       impl.Ssv000005I.WdrwlMthd,    //Withdrawal method 1- password;
		NeedPswFlg:      impl.Ssv000005I.NeedPswFlg,   //Do you need to test Y- Do you need to test Y;N- You don't need to check
		AccPsw:          impl.Ssv000005I.AccPsw,       //If the account password is Y, this field must be filled
		TranAmt:         impl.Ssv000005I.TranAmt,      //The amount of
		GoingFunds:      impl.Ssv000005I.GoingFunds,   //1- Customer account;2- Internal account
		CltAgrmt:        impl.Ssv000005I.CltAgrmt,     //Collection Contract No.
		CltAgrmtTyp:     impl.Ssv000005I.CltAgrmtTyp,  //Collection contract Type
		CltInAcc:        impl.Ssv000005I.CltInAcc,     //Transfer to internal account
		CltMdsNM:        impl.Ssv000005I.CltMdsNM,     //Recipient's media number
		CltMdsTyp:       impl.Ssv000005I.CltMdsTyp,    //Recipient medium type
		CltCusId:        impl.Ssv000005I.CltCusId,     //Recipient customer Number
		CltCusTy:        impl.Ssv000005I.CltCusTy,
		CltCUR:          impl.Ssv000005I.CltCUR, //The receiver's coin
		CltCashTranFlag: impl.Ssv000005I.CltCashTranFlag,
		CltUsgCod:       impl.Ssv000005I.CltUsgCod,
		CltAccuntNme:    impl.Ssv000005I.CltAccuntNme,  //Account name
		Postscript:      impl.Ssv000005I.Postscript,    //Transfer a postscript
		DlyArrvlAFlag:   impl.Ssv000005I.DlyArrvlAFlag, //Delay to account mark 1- Real-time to account;2- Delay to account
		TranTyp:         impl.Ssv000005I.TranTyp,       //Transaction mode 1- Online;2 - the batch
		BatchNm:         impl.Ssv000005I.BatchNm,
		AmtFreAuth:      impl.Ssv000005I.AmtFreAuth, //Amount freeze Authorization mark Y- has been authorized
		KeyVersion:      impl.Ssv000005I.KeyVersion,
	}
	//pack request
	reqBody, err := ssv100014I.PackRequest()
	if nil != err {
		return err
	}
	//call das get serial no
	resBody, err := impl.RequestSyncServiceElementKey(constant.DLS_TYPE_CMM, constant.DLS_TYPE_ELEMENT_ID, constant.SV100014, reqBody)
	if err != nil {
		return err
	}

	sv1000014O := models.SSV1000014O{}
	err = sv1000014O.UnPackResponse(resBody)
	if err != nil {
		return err
	}
	impl.Ssv100014O = &sv1000014O

	return nil
}

//Registration and accounting flow
func (impl *Ssv0000005Impl) InsertAcctFlow() error {
	TxnFlow := []models.TxnAccountingFlows{
		{
			TxnDate:        time.Now().Format("2006-01-02"),            //tran date
			GlobalBizSeqNo: impl.SrcAppProps[constant2.GLOBALBIZSEQNO], //Global service flow number
			TranBrno:       impl.SrcAppProps[constant2.TXDEPTCODE],     //Trading firms
			TranChannel:    "SV",                                       //Transaction initiation channel APP
			BussSys:        "IL",                                       //The business system
			TranReqCode:    "SV000015",                                 //Transaction request code
			HandleType:     "001",                                      //Processing type
			HandleTypeSeq:  1,                                          //Deal with type ordinals
			AcctDiff:       "0",                                        //This account is divided into 0 external accounts and 1 external account
			AcctingOrgId:   impl.SrcAppProps[constant2.TXDEPTCODE],     //The accounting bank of the party
			AcctNo:         "",                                         //Account number of our client
			ContNo:         "",                                         //Our contract Number
			Cur:            "",                                         //His own currency
			Amt:            impl.Ssv000005I.TranAmt,                    //Local amount
			QuivalenceAmt:  impl.Ssv000005I.TranAmt,                    //Equivalent amount of this party
			ExchRate:       0,                                          //His own currency
			TranSerialNo:   impl.SrcAppProps[constant2.GLOBALBIZSEQNO], //Original transaction serial number
			SrcTranDate:    time.Now().Format("2006-01-02"),            //Original trading date
		},
	}
	//Get the customer number, contract number and currency of the external account
	if impl.Outssv90004O != nil {
		TxnFlow[0].ContNo = impl.Outssv90004O.Records[0].AgreementID
		TxnFlow[0].Cur = impl.Ssv000005I.PayCUR
		TxnFlow[0].AcctNo = impl.Outssv90004O.Records[0].AccAcount
	} else if impl.Inssv90004O != nil {
		TxnFlow[0].ContNo = impl.Inssv90004O.Records[0].AgreementID
		TxnFlow[0].Cur = impl.Ssv000005I.CltCUR
		TxnFlow[0].AcctNo = impl.Inssv90004O.Records[0].AccAcount
	}

	ssv100020I := models.SSV1000020I{
		TxnAccountingFlows: TxnFlow,
	}
	//pack request
	reqBody, err := ssv100020I.PackRequest()
	if nil != err {
		return err
	}
	//call das get serial no
	resBody, err := impl.RequestSyncServiceElementKey(constant.DLS_TYPE_CMM, constant.DLS_TYPE_ELEMENT_ID, constant.SV100020, reqBody)
	if err != nil {
		return err

	}

	sv1000020O := models.SSV1000020O{}
	err = sv1000020O.UnPackResponse(resBody)
	if err != nil {
		return err
	}

	return nil
}

//Call transfer flow update
func (impl *Ssv0000005Impl) UpdateTransferFlow(CustId string, tranResult string) error {
	ssv90010I := models.SSV9000010I{
		GlobalBizseqNo: impl.SrcAppProps[constant2.GLOBALBIZSEQNO],
		TranResult:     tranResult, //处理结果
	}
	//pack request
	reqBody, err := ssv90010I.PackRequest()
	if nil != err {
		return err
	}
	//call das get serial no
	resBody, err := impl.RequestSyncServiceElementKey(constant.DLS_TYPE_CUS, CustId, constant.SV900010, reqBody)
	if err != nil {
		return err
	}

	sv900009O := models.SSV9000009O{}
	err = sv900009O.UnPackResponse(resBody)
	if err != nil {
		return err
	}
	return nil
}

//Notice of moving account information
func (impl *Ssv0000005Impl) MoveAcctNotice() error {
	ssv100019I := models.SSV1000019I{
		SysId:             "1", //Channel logo tentative ILNS Lego Smart Loan
		BizSn:             "1", //Service number
		ObjectSendList:    nil, //Sends a collection of object contents
		ObjectReceiveList: nil, //Receives a collection of object contents
		LoanProdtNo:       "1", //Product no.
		ApplyScena:        "1", //Applicable scenario category
		TextTmplTypCd:     "1", //Template business type code 001-Transaction Statement 1002-Transaction History 1003-Repayment Plan 1004-Late Payment Notification
		OrgId:             "1", //Agency id
		Area:              "1", //region
		ChannelNumber:     "1", //Channel no.
		EventId:           "1", //Event ID TopicID
		SendObjectType:    "1", //Type of sending object (01- agency, 02- Account Manager)
		TextTmplDetailCd:  "1", //Template business type fine class
		MsgDate:           "1", //The date of
		MsgTime:           "1", //time
		AttachFileIDList:  nil, //Attachment file ID list
		Body:              nil, //content
	}

	//pack request
	reqBody, err := ssv100019I.PackRequest()
	if nil != err {
		return err
	}
	//call das get serial no
	err = impl.RequestAsyncServiceElementKey(constant.DLS_TYPE_CMM, constant.DLS_TYPE_ELEMENT_ID, constant.SV100019, reqBody)
	if err != nil {
		return err
	}

	return nil
}

func (impl *Ssv0000005Impl) ConfirmSsv0000005(ssv0000005I *models.SSV0000005I) (ssv0000005O *models.SSV0000005O, err error) {
	//TODO Business  confirm Process
	log.Debug("Start confirm ssv0000005")
	return nil, nil
}

func (impl *Ssv0000005Impl) CancelSsv0000005(ssv0000005I *models.SSV0000005I) (ssv0000005O *models.SSV0000005O, err error) {
	//TODO Business  cancel Process
	log.Debug("Start cancel ssv0000005")
	return nil, nil
}
